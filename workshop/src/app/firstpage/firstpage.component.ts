import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl,FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Student } from '../student';
import { DataService } from '../data.service';
@Component({
  selector: 'app-firstpage',
  templateUrl: './firstpage.component.html',
  styleUrls: ['./firstpage.component.css']
})
export class FirstpageComponent implements OnInit {
  student: Student | undefined;

  myForm = this.formBuilder.group({
    firstName : ['', Validators.required],
    surName : ['', Validators.required],
    ages : ['', Validators.required],
    year : ['', Validators.required],
    university : ['', Validators.required],
    facebook : [''],
  });
  constructor(
    private formBuilder: FormBuilder,
    private dataService:DataService
  ) { }

  ngOnInit(): void {

    if(this.dataService.sharedData != null){
      this.myForm.setValue({
        firstName: this.dataService.sharedData['firstName'],
        surName: this.dataService.sharedData['surName'],
        ages: this.dataService.sharedData['ages'],
        year: this.dataService.sharedData['year'],
        university: this.dataService.sharedData['university'],
        facebook: this.dataService.sharedData['facebook'],
      });
    }
  }
  onSubmit():void{
    if(this.myForm.valid){
      
    }
    console.log(this.myForm.value);
    this.student = this.myForm.value;
    this.dataService.sharedData = this.student;
  }
  




}
