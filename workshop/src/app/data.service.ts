import { Injectable } from '@angular/core';
import { Student } from './student';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  sharedData: Student | undefined;
  constructor() { }
}
