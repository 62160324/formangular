export interface Student{
    firstName:string,
    surName:string,
    ages:number,
    year : string,
    university : string,
    facebook : string
}