import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DataService } from '../data.service';
import { Student } from '../student';

@Component({
  selector: 'app-secondpage',
  templateUrl: './secondpage.component.html',
  styleUrls: ['./secondpage.component.css']
})
export class SecondpageComponent implements OnInit {
  info:Student | undefined;
  constructor(
    private dataService:DataService
  ) { }

  ngOnInit(): void {
    this.getData();
  }
  getData():void{
    this.info = this.dataService.sharedData;
    console.log(this.info);
  }
  
}
